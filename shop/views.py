from rest_framework import generics

from shop.models import Shop
from shop.serializers import ShopSerializer


class ShopsList(generics.ListCreateAPIView):
    """
    List of all shops
    """
    queryset = Shop.objects.all()
    serializer_class = ShopSerializer


class ShopDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Shop.objects.all()
    serializer_class = ShopSerializer


