from rest_framework import serializers

from shop.models import Shop


class ShopSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source="owner.user.email")

    class Meta:
        model = Shop
        fields = ("id", "name", "domain", "email", 'owner')
