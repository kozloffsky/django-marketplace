from django.contrib import admin

from shop.models import Account, Shop, CustomerAccount


class AccountAdmin(admin.ModelAdmin):
    pass


class CustomerAccountAdmin(admin.ModelAdmin):
    pass


class ShopAdmin(admin.ModelAdmin):
    pass


admin.site.register(Account, AccountAdmin)
admin.site.register(CustomerAccount, CustomerAccountAdmin)
admin.site.register(Shop, ShopAdmin)


