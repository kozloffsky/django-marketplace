from django.contrib.auth.models import User
from django.db import models
from django.db.models.deletion import CASCADE


class Account(models.Model):
    """
    Shop owner Account
    """
    user = models.OneToOneField(User, on_delete=CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class CustomerAccount(models.Model):
    """
    Shop customer account
    """
    user = models.ForeignKey(User, on_delete=CASCADE)
    shop = models.ForeignKey("Shop", on_delete=CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Shop(models.Model):
    """
    The shop object is a collection of the general settings and information about the shop.
    Only the shop owner can edit this information
    """

    # The name of the shop.
    name = models.CharField(max_length=254, null=False)

    # The contact email used for communication between marketplace and the shop owner.
    email = models.EmailField()

    # Api endpoint domain for this shop. For ex apple.marketplace.ua
    domain = models.CharField(max_length=254, null=False)

    # Account that owns this shop
    owner = models.ForeignKey(Account, related_name="shops", on_delete=CASCADE, null=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
