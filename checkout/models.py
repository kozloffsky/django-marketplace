from django.db import models
from django.db.models.deletion import CASCADE

from product.models import ProductVariant
from shop.models import Shop, CustomerAccount


class Checkout(models.Model):
    """
    Checkout folds all shopping cart information
    """
    shop = models.ForeignKey(Shop, on_delete=CASCADE, related_name="checkouts", null=False, blank=False)
    customer = models.ForeignKey(CustomerAccount, on_delete=CASCADE, related_name="checkouts")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class LineItem(models.Model):
    """
    Checkout item contains information of current item for sell
    like variant id and quantity
    """
    product_variant = models.ForeignKey(ProductVariant, on_delete=CASCADE, related_name="line_items", null=False,
                                        blank=False)
    quantity = models.BigIntegerField(null=False, blank=False, default=1)
