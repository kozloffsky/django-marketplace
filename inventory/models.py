from django.db import models
from django.db.models.deletion import CASCADE

from product.models import ProductVariant


class Warehouse(models.Model):
    """
    Objects represents physical stores, warehousies or any facilities that have
    actual goods
    """
    name = models.CharField(max_length=254, null=False, blank=False)


class InventoryItem(models.Model):
    """
    Represents actual goods with one to one relation to product variant
    """
    product_variant = models.OneToOneField(ProductVariant, related_name="inventory", null=False, blank=False,
                                           on_delete=CASCADE)
    warehouses = models.ManyToManyField(Warehouse, related_name="items", blank=False)
    quantity = models.BigIntegerField(null=False, blank=False, default=0)
