from django.db import models
from django.db.models.deletion import CASCADE


class Product(models.Model):

    title = models.CharField(max_length=254, null=False)

    description = models.TextField(blank=False, null=False)

    def __str__(self):
        return self.title


class ProductVariant(models.Model):

    product = models.ForeignKey(Product, related_name="variants", on_delete=CASCADE, null=False)

    title = models.CharField(max_length=254, blank=False, null=False)

    sku = models.CharField(max_length=254, blank=False, null=False)

    options = models.ManyToManyField("ProductOptionValue", related_name="variants")

    def __str__(self):
        return self.title


class ProductOption(models.Model):

    product = models.ForeignKey(Product, related_name="options", on_delete=CASCADE, null=False)

    name = models.CharField(max_length=254,  null=False, blank=False)

    def __str__(self):
        return self.product.title + " " + self.name


class ProductOptionValue(models.Model):

    option = models.ForeignKey(ProductOption, related_name="values", on_delete=CASCADE, null=False)

    # Serialized value of this option
    data = models.CharField(max_length=254, blank=False, null=False)
    # TODO: implement types of values and its serialization

    def __str__(self):
        return self.option.product.title+" "+self.option.name + " " + self.data
