from django.contrib import admin

from product.models import Product, ProductVariant, ProductOption, ProductOptionValue


class ProductAdmin(admin.ModelAdmin):
    pass


class ProductVariantAdmin(admin.ModelAdmin):
    pass


class ProductOptionAdmin(admin.ModelAdmin):
    pass


class ProductOptionValueAdmin(admin.ModelAdmin):
    pass


admin.site.register(Product, ProductAdmin)
admin.site.register(ProductVariant, ProductVariantAdmin)
admin.site.register(ProductOption, ProductOptionAdmin)
admin.site.register(ProductOptionValue, ProductOptionValueAdmin)
