from django.db import models
from django.db.models.deletion import CASCADE

from product.models import Product
from shop.models import Shop


class Catalog(models.Model):
    """
    Catalog is collection of products that can be shared between multiple shops of one owner.
    One catalog for one shop.
    """
    shop = models.ForeignKey(Shop, related_name='catalogs', on_delete=CASCADE, null=False)
    products = models.ManyToManyField(Product, related_name="catalogs")
    name = models.CharField(max_length=254, null=False, blank=False)
