from django.urls import path

from management.views import Dashboard

urlpatterns = [
    path('', Dashboard.as_view())
]